public class PercolationStats {

    private int numberOfSimulations;
    private double mu;
    private double sigma;

    /* Constructor */
    public PercolationStats(int N, int T)
    {
        if ((N < 1) || (T < 1))
            throw new IllegalArgumentException("N and T should be greater than 0");

        int openSitesToPercolate = 0;
        double totalSites = (double) N*N;
        double[] percolationThreshold = new double[T];
        numberOfSimulations = T;
        for (int i = 0; i < T; i++) {
            openSitesToPercolate = runSimulation(N);
            percolationThreshold[i] = openSitesToPercolate / totalSites;
        }
        mu = StdStats.mean(percolationThreshold);
        if (T > 1)
            sigma = StdStats.stddev(percolationThreshold);
        else
            sigma = Double.NaN;
    }

    /* Sample mean of percolation threshold */
    public double mean()
    {
        return mu;
    }

    /* Sample standard deviation of percolation threshold */
    public double stddev()
    {
        return sigma;
    }

    /* Lower bound of the 95% confidence interval */
    public double confidenceLo()
    {
        return mu - (1.96 * sigma)/Math.sqrt(numberOfSimulations);
    }

    /* Upper bound of the 95% confidence interval */
    public double confidenceHi()
    {
        return mu + (1.96 * sigma)/Math.sqrt(numberOfSimulations);
    }

    /* Test client */
    public static void main(String[] args)
    {
        if (args.length != 2) {
            StdOut.println("Wrong number of arguments. Usage:");
            StdOut.println("  java PercolationStats N T");
        } else {
            int N = Integer.parseInt(args[0]);
            int T = Integer.parseInt(args[1]);
            PercolationStats p = new PercolationStats(N, T);
            StdOut.println("mean                    = " + p.mean());
            StdOut.println("stddev                  = " + p.stddev());
            StdOut.println("95% confidence interval = " + p.confidenceLo() + ", " + p.confidenceHi());
        }
    }

    /* Run a single simulation for a grid, returning the number of sites that
       have to be opened before the grid percolates */
    private int runSimulation(int N)
    {
        int i, j;
        int sitesOpen = 0;
        Percolation grid = new Percolation(N);
        while (!grid.percolates()) {
            i = StdRandom.uniform(N) + 1;
            j = StdRandom.uniform(N) + 1;
            if (!grid.isOpen(i, j)) {
                sitesOpen += 1;
                grid.open(i, j);
            }
        }
        return sitesOpen;
    }
}
