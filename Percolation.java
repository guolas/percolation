public class Percolation {
   /* Stores the grid with a ficticious top element*/
   private WeightedQuickUnionUF grid;
   /* Stores the auxiliary grid with both a ficticious top and a 
      ficticious bottom sites*/
   private WeightedQuickUnionUF auxGrid;
   /* Stores the status of each site in the grid:
      false -> site is closed
      true  -> site is open */
   private boolean[] gridStatus;

   /* Size of the (side of the) grid */
   private int N;

   /* Constructor */
   public Percolation(int N)
   {
      grid = new WeightedQuickUnionUF(N*N + 1);
      auxGrid = new WeightedQuickUnionUF(N*N + 2);
      gridStatus = new boolean[N*N + 2];
      for (int i = 0; i <= N*N + 1; i++)
      {
         gridStatus[i] = false;
      }
      gridStatus[0] = true; // top element
      gridStatus[N*N+1] = true; // bottom element
      this.N = N;
   }

   /* Opens the position at row `i` and column `j` */
   public void open(int i, int j)
   {
      indexValidation(i, j);
      int currentLocation = xyTo1D(i, j);
      gridStatus[currentLocation] = true;
      connectToOpenNeighbors(i, j);
   }

   /* Checks if a position is open */
   public boolean isOpen(int i, int j)
   {
      indexValidation(i, j);
      int currentLocation = xyTo1D(i, j);
      return gridStatus[currentLocation];
   }

   /* Checks if a position is full */
   public boolean isFull(int i, int j)
   {
      indexValidation(i, j);
      int currentLocation = xyTo1D(i, j);
      return grid.connected(0, currentLocation);
   }

   /* Checks if the grid is percolated */
   public boolean percolates()
   {
      return auxGrid.connected(0, N*N + 1);
   }

   /* Connect to open neighbors */
   private void connectToOpenNeighbors(int i, int j)
   {
      int neighbor;
      int currentLocation = xyTo1D(i, j);
      if (j > 1) { /* left neighbor */
         neighbor = xyTo1D(i, j - 1);
         if (gridStatus[neighbor]) {
            grid.union(currentLocation, neighbor);
            auxGrid.union(currentLocation, neighbor);
         }
      }
      if (j < N) { /* right neighbor */
         neighbor = xyTo1D(i, j + 1);
         if (gridStatus[neighbor]) {
            grid.union(currentLocation, neighbor);
            auxGrid.union(currentLocation, neighbor);
         }
      }
      if (i > 1) { /* top neighbor */
         neighbor = xyTo1D(i - 1, j);
         if (gridStatus[neighbor]) {
            grid.union(currentLocation, neighbor);
            auxGrid.union(currentLocation, neighbor);
         }
      } else { /* First row, so connect it to the TOP position */
         grid.union(currentLocation, 0);
         auxGrid.union(currentLocation, 0);
      }
      if (i < N) { /* bottom neighbor */
         neighbor = xyTo1D(i + 1, j);
         if (gridStatus[neighbor]) {
            grid.union(currentLocation, neighbor);
            auxGrid.union(currentLocation, neighbor);
         }
      } else { /* Last row, so connect it to the BOTTOM position */
         auxGrid.union(currentLocation, N*N + 1);
      }
   }

   /* Mapping from 2D to 1D */
   private int xyTo1D(int i, int j)
   {
      return (i-1)*N + j;
   }

   /* Validate the indexes, throws an exception if not valid */
   private void indexValidation(int i, int j)
   {
      if (i < 1 || i > N || j < 1 || j > N)
         throw new IndexOutOfBoundsException("Position (" + i + ", " + j + ") out of bounds");
   }

   /* Main, for testing purposes */
   public static void main(String[] args)
   {
   }
}
